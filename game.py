from random import randint

name = input("Hi!, What is your name?")

for guess_num in range(6):
    guess_count=0
    m_number = randint(1, 12)
    y_number = randint(1924, 2004)

    print(f"Guess {guess_num} :",  name , "were you born in ",
          m_number, "/", y_number, "?")

    response = input("yes or no? ")

    if response == "yes":
     print("I knew it!")
     exit()
    elif guess_num == 5:
     print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")
